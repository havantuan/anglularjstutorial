import {Component, OnInit} from '@angular/core';
import {Observable, of, Subject} from 'rxjs';
import {debounce, debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {Movie} from '../models/movie';
import {MovieService} from '../movie.service';

@Component({
    selector: 'app-movie-search',
    templateUrl: './movie-search.component.html',
    styleUrls: ['./movie-search.component.css']
})
export class MovieSearchComponent implements OnInit {
    movies$: Observable<Movie[]>;
    private searchSubject = new Subject<string>();

    constructor(private moviceService: MovieService) {
    }

    search(searchedString: string): void {
        this.searchSubject.next(searchedString);
    }

    ngOnInit() {
        this.movies$ = this.searchSubject.pipe(
            debounceTime(300),
            distinctUntilChanged(), //ingore new string if same as previous string
            switchMap((searchedString: string) => this.moviceService.searchMovie(searchedString))
        );
    }

}
