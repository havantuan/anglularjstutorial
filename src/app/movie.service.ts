import {Injectable} from '@angular/core';
import {fakeMovies} from './fake-movies';
import {Movie} from './models/movie';
import {MessageService} from './message.service';
//Get data asynchronously with Observable
import {Observable} from 'rxjs';
import {of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
};
@Injectable()
export class MovieService {
    private moviesURL = 'http://localhost:3000/movies';

    getMovies(): Observable<Movie[]> {
        // this.messageService.add(`${ new Date().toLocaleString()}. Get movie list`);
        // return of(fakeMovies);
        return this.http.get<Movie[]>(this.moviesURL).pipe(
            tap(receivedMovies => console.log(`receivedMovies = ${JSON.stringify(receivedMovies)}`)),
            catchError(err => of([]))
        );
    }

    getMovieByParam(id: number): Observable<Movie> {
        // return of(fakeMovies.find(movie => movie.id === id));
        const url = `${this.moviesURL}/${id}`;
        return this.http.get<Movie>(url).pipe(
            tap(selectedMovie => console.log(`selectd Movide ${JSON.stringify(selectedMovie)}`)),
            catchError(error => of(new Movie()))
        );
    }
    updateMovie(movie: Movie): Observable<any> {
        return this.http.put(`${this.moviesURL}/${movie.id}`, movie, httpOptions).pipe(
          tap(updateMovie => console.log(`Update movie ${JSON.stringify(updateMovie)}`)),
          catchError(err => of(new Movie()))
        );
    }
    addMovie(newMovie: Movie): Observable<Movie> {
        return this.http.post(this.moviesURL, newMovie, httpOptions).pipe(
            tap((movie: Movie) => console.log(`Data insert is ${JSON.stringify(movie)}`)),
            catchError(error => of(new Movie()))
        );
    }
    removeMovie(movieId: number): Observable<Movie> {
        const url = `${this.moviesURL}/${movieId}`;
        return this.http.delete<Movie>(url, httpOptions).pipe(
            tap(_ => console.log(`Delete Movie with ID ${movieId}`)),
            catchError(err => of(null))
        );
    }
    searchMovie(typeString: string): Observable<Movie[]> {
        if (!typeString.trim()) {
            return of([]);
        }
        return this.http.get<Movie[]>(`${this.moviesURL}?name_like=${typeString}`).pipe(
          tap(result => console.log(`Data search is ${JSON.stringify(result)}`)),
          catchError(err => of(null))
        );
    }
    constructor(
        private http: HttpClient,
        public messageService: MessageService) {
    }
}
