import { Component, OnInit } from '@angular/core';
import { Movie } from '../models/movie';
// import { fakeMovies } from '../fake-movies';
import {MovieService} from '../movie.service';
@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  // movie: Movie = {
  //   id: 1,
  //   name: "Star Wars",
  //   releaseYear: 1977
  // }
  // movies = fakeMovies;
  movies: Movie[];
  constructor(private movieService: MovieService) { }
  getMoviesFromService(): void {
    // this.movies = this.movieService.getMovies();
    this.movieService.getMovies().subscribe(
      (updatedMovies) => {
        this.movies = updatedMovies;
      }
    );
  }
  ngOnInit() {
    this.getMoviesFromService();
  }
  // selectedMovie: Movie;
  // onSelect(movie: Movie): void {
  //   this.selectedMovie = movie;
  //   console.log(`dataa iss ${JSON.stringify(this.selectedMovie)}`);
  // }
    addMovie(name: string, releaseYear: string): void {
        name = name.trim();
        if (Number.isNaN(Number(releaseYear)) || !name || Number(releaseYear) === 0) {
            alert('Name must blank, ReleaseYear must be a number');
            return;
        }
        const newMovie: Movie = new Movie();
        newMovie.name = name;
        newMovie.releaseYear = Number(releaseYear);
        this.movieService.addMovie(newMovie).subscribe(
            insertMovie => {
              this.movies.push(insertMovie);
            }
        );
    }
    deleteMovie(id: number): void {
      this.movieService.removeMovie(id).subscribe(_ => {
        this.movies = this.movies.filter(eachMovie => eachMovie.id !== id);
      });
    }
}
